def reversenum(num):
  rev = 0
  while num > 0:
    temp = num % 10
    num //=10
    rev *= 10
    rev += temp
  return rev

def cutnum(num):
  num1 = int(num)
  first = reversenum(num1)
  num = str(num)
  num2 = num.split('.')
  num2 = int(num2[1])
  second = reversenum(num2)
  total = float(str(first) + '.' + str(second))
  return total

num1 = float(input('Введите первое число: '))
num2 = float(input('Введите второе число: '))
rev1 = cutnum(num1)
rev2 = cutnum(num2)
print()
print(rev1)
print(rev2)
print()
sum = rev1 + rev2
print(sum)
