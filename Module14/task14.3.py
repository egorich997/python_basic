
def summ(num):
    while num != 0:
        temp = num % 10
        num //= 10
    return temp

def quant(num):
    temp = 0
    while num != 0:
        temp += 1
        num //= 10
    return temp


num = int(input('Введите число: '))
summ = summ(num)
print('Сумма цифр числа:', summ)
quant = quant(num)
print('Количество цифр числа:', quant)
print('Разность суммы и количества:', summ - quant)